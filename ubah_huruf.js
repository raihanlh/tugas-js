
/**
 * 
 * Diberikan function ubahHuruf(kata) yang akan menerima satu parameter berupa string.
 * Function akan me-return sebuah kata baru dimana setiap huruf akan digantikan dengan huruf alfabet setelahnya.
 * Contoh, huruf a akan menjadi b, c akan menjadi d, k menjadi l, dan z menjadi a.
 * 
 */


function ubahHuruf(kata) {
    // you can only write your code here!
    result = "";

    for (let i = 0; i < kata.length; i++) {
        result += String.fromCharCode(kata[i].charCodeAt()+1 > 122 ? 97 + (kata[i].charCodeAt() % 122) : kata[i].charCodeAt()+1)
    }
    return result
}
  
// TEST CASES
console.log(ubahHuruf('wow')); // xpx
console.log(ubahHuruf('developer')); // efwfmpqfs
console.log(ubahHuruf('javascript')); // kbwbtdsjqu
console.log(ubahHuruf('keren')); // lfsfo
console.log(ubahHuruf('semangat')); // tfnbohbu
